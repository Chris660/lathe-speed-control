import time
import machine as rpi

print("starting: rp2040 @", rpi.freq() / 1e6, "MHz")

# Global configuration parameters - edit these as needed
START_STOP_GPIO = 17  # board pin 22
MOTOR_PWM_GPIO  = 18  # board pin 24
STATUS_LED_GPIO = 25  # board internal
SPEED_POT_GPIO  = 27  # board pin 32

MOTOR_PWM_HZ = 25_000     # 7Hz .. 125 MHz
MOTOR_PWM_MIN_DUTY = 0.1  # 0.0 .. 1.0
MOTOR_PWM_MAX_DUTY = 0.6

# GPIO Pad control defs - needed to set output drive level which
# is not yest supported in micropython for the pico.
PAD_BANK0_BASE  = 0x4001c000
PAD_GPIO_OFFSET = 0x04
PAD_GPIO_DRIVE__2mA = 0 << 4
PAD_GPIO_DRIVE__4mA = 1 << 4
PAD_GPIO_DRIVE__8mA = 2 << 4
PAD_GPIO_DRIVE_12mA = 3 << 4
PAD_GPIO_DRIVE_MASK = 0x000000CF

# Sanity check config.
assert 7 <= MOTOR_PWM_HZ <= 125_000_000
assert 0.0 <= MOTOR_PWM_MIN_DUTY <= MOTOR_PWM_MAX_DUTY <= 1.0
MOTOR_PWM_DUTY_RANGE = MOTOR_PWM_MAX_DUTY - MOTOR_PWM_MIN_DUTY

class Motor:
    STOPPED = 0
    RUNNING = 1

    # Calculates the desired motor PWM duty (0..1) for the given `set_speed`
    @staticmethod
    def speed_to_pwm_u16(set_speed):
        duty_f = MOTOR_PWM_MIN_DUTY + set_speed * MOTOR_PWM_DUTY_RANGE
        assert 0.0 <= duty_f <= MOTOR_PWM_MAX_DUTY
        # The h/w expects an unsigned 16-bit duty value in the range 0..65535
        duty_u16 = round(duty_f * 65535)
        assert 0 <= duty_u16 <= 65535
        return duty_u16

    def __init__(self, gpio):
        # Initialise the PWM output pin.
        self.pin = rpi.Pin(gpio,
                           mode=rpi.Pin.OUT,
                           pull=rpi.Pin.PULL_DOWN,
                           value=0)
        # Set drive level - not yet supported by micropython for RP2 AFAICS.
        # The maximum setting of 12mA keeps the pulse output above 3.3V
        # when driving a 330 ohm dummy load (so ~10mA).
        # NOTE: need to read-modify-write all 32-bits.
        pad_ctl_addr = PAD_BANK0_BASE + PAD_GPIO_OFFSET + 4 * gpio
        gpio_cfg_old = rpi.mem32[pad_ctl_addr]
        gpio_cfg_new = (gpio_cfg_old & PAD_GPIO_DRIVE_MASK) | PAD_GPIO_DRIVE_12mA
        rpi.mem32[pad_ctl_addr] = gpio_cfg_new
        print("set drive strength for gpio {}: 0x{:02x} -> 0x{:02x}"
              .format(gpio, gpio_cfg_old, rpi.mem32[pad_ctl_addr]))

        self.pwm = rpi.PWM(self.pin)
        self.pwm.duty_u16(0)
        self.pwm.freq(MOTOR_PWM_HZ)
        self.speed_setpoint = 0.0
        self.state = Motor.STOPPED

    def update(self, now):
        if self.state == Motor.RUNNING:
            # Determine target duty cycle for the current set speed
            # NB. the value read back from duty_u16() is not necessarily
            # the value we last set.  It appears there is some coarser
            # quantisation / rounding going on under the covers.
            current_duty = self.pwm.duty_u16()
            target_duty = Motor.speed_to_pwm_u16(self.speed_setpoint)
            if current_duty < target_duty:
                # XXX: crappy rate limit - should either calc from `now`,
                # or preferably trigger these updates using a separate
                # fixed frequency periodic timer.
                self.pwm.duty_u16(min(current_duty + 16, target_duty))
            else:
                self.pwm.duty_u16(target_duty)
        else:
            self.pwm.duty_u16(0) # no ramp down

    def stop(self):
        self.state = Motor.STOPPED
        self.pwm.duty_u16(0) # no ramp down

    def start(self):
        self.state = Motor.RUNNING

    def set_speed(self, fraction):
        assert 0.0 <= fraction <= 1.0
        self.speed_setpoint = fraction
        self.speed_settime = time.ticks_ms()

    def duty(self):
        return self.pwm.duty_u16() / 65535.0

    def is_running(self):
        return self.state == Motor.RUNNING


class StatusLED:
    # TODO: use another PWM output for the LED blinking.
    def __init__(self, gpio):
        self.pin = rpi.Pin(gpio, rpi.Pin.OUT)
        self.period_ms = [1000, 0] # on, off
        self.pin.value(1)
        self.sched_ticks = 0

    def update(self, now=time.ticks_ms()):
        if time.ticks_diff(self.sched_ticks, now) <= 0:
            next_ms = self.period_ms[self.pin.value()]
            if next_ms > 0:
                self.pin.toggle()
            self.sched_ticks = time.ticks_add(now, next_ms)

    def set_on_off_ms(self, on_ms, off_ms):
        self.period_ms = [on_ms, off_ms]

    def on(self):
        self.pin.value(1)
        self.period_ms = [1000, 0]

    def off(self):
        self.pin.value(0)
        self.period_ms = [0, 1000]


# H/W configuration and initialisation
speed_pot = rpi.ADC(rpi.Pin(SPEED_POT_GPIO))
motor = Motor(MOTOR_PWM_GPIO)
start_stop = rpi.Pin(START_STOP_GPIO, rpi.Pin.IN, pull=rpi.Pin.PULL_UP)
status_led = StatusLED(STATUS_LED_GPIO)

# Returns the requested speed in the range 0.0 .. 1.0
# XXX: integer arithmetic would be faster & cleaner, but we don't need the speed.
def read_set_speed():
    # TODO: filtering of the input, for now just truncate the
    # noisy lower bits before converting to the float output.
    speed = (speed_pot.read_u16() // 1024) / 63.0
    assert 0.0 <= speed <= 1.0
    return speed

def run():
    motor.stop()

    now = time.ticks_ms()
    status_sched = now
    start_stop_debounce_sched = now
    start_stop_last_state = start_stop.value()

    last_duty = -1.0
    speed = 0.0

    # Main programme loop
    while True:
        now = time.ticks_ms()

        # Check the start/stop button state
        # Start a debounce timer after a high->low transition is detected
        start_stop_state = start_stop.value()
        if start_stop_state == 0:
            if start_stop_last_state == 1 \
                    and time.ticks_diff(start_stop_debounce_sched, now) <= 0:
                if motor.is_running():
                    print("stop")
                    motor.stop()
                    status_led.set_on_off_ms(1000, 0)
                else:
                    print("start")
                    motor.start()
                    status_led.set_on_off_ms(500, 500)

            # The debounce timer only starts when the button is up.
            start_stop_debounce_sched = time.ticks_add(now, 200)

        start_stop_last_state = start_stop_state

        if motor.is_running():
            speed = read_set_speed()
            motor.set_speed(speed)
            motor.update(now)

        status_led.update(now)

        # Periodically print status changes to the console.
        if time.ticks_diff(status_sched, now) <= 0:
            status_sched = time.ticks_add(status_sched, 200)
            duty = motor.duty()
            if duty != last_duty:
                last_duty = duty
                print("set_speed:", speed, ", pwm_duty:", duty)

try:
    run()
except Exception as e:
    # Fail safe with the motor off.
    motor.stop()
    print('unhandled exception: ', str(e))

    # Slow blink to indicate an error
    status_led.set_on_off_ms(200, 1800)
    while True:
        status_led.update()
        time.sleep_ms(50)
